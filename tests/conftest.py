import pytest
from mp_driver import MPDriver
from tests.config import *

@pytest.fixture(scope='module')
def driver():
    d = MPDriver(MP_ENV)
    try:
        d.mp_login(MP_USERNAME, MP_PASSWORD)
    except:
        pass
    else:
        yield d
    finally:
        d.quit()
