import pytest
from tests.config import MP_ENV
from selenium_mp.common.utils import get_uri


def test_uri(driver):
    uri = 'PROCESS_APPLICATIONS.ACAD_TST_RSLT_ADMA.GBL'
    expected = (f"https://{MP_ENV}.dsc.umich.edu/psp/"
                f"{MP_ENV}op/EMPLOYEE/SA/c/{uri}")
    get_uri(driver, uri)
    assert driver.current_url == expected
