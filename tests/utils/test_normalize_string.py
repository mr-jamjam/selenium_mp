import pytest
from selenium_mp.common.utils import normalize_string


testdata = [('Processes', 'processes'),
            ('Foo/Bar', 'foo_bar'),
            ('   ', ''),
            ('1234abc', 'abc'),
            ('Seq.', 'seq')]

@pytest.mark.parametrize('input,expected', testdata)
def test_normalize_string(input, expected):
    actual = normalize_string(input)
    assert actual == expected
