import pytest
from selenium_mp.common.elements import Grid


@pytest.fixture(scope='function')
def ext_transcript_grid(driver):
    ADM_APPL_NBR = '01880461'
    URI = 'PROCESS_APPLICATIONS.ACAD_HISTORY_ADMA.GBL'
    ENV = driver.env
    url = f'https://{ENV}.dsc.umich.edu/psc/{ENV}op/EMPLOYEE/HRMS/c/{URI}'
    driver.get(url)
    driver.find_element_by_id('ADM_APPL_SCTY_ADM_APPL_NBR')\
          .send_keys(ADM_APPL_NBR)
    driver.find_element_by_id('#ICSearch').click()
    driver.mp_wait(180)
    return Grid(driver, ('id', 'M_RA_TRANSCRIPT$scrolli$0'))

def test_tab_names(ext_transcript_grid):
    """Grid.tab_names should return a list of the visible tabs."""
    expected = ['Location', 'Detail']
    actual = ext_transcript_grid.tab_names
    assert expected == actual

def test_tabs(ext_transcript_grid):
    assert len(ext_transcript_grid.tabs) == 2

def test_expand_tabs(ext_transcript_grid):
    initial_columns = list(ext_transcript_grid.columns())
    ext_transcript_grid.expand_tabs()
    final_columns = list(ext_transcript_grid.columns())
    assert len(final_columns) > len(initial_columns)

def test_click_tab_by_name_raises_with_bad_tabname(ext_transcript_grid):
    with pytest.raises(ValueError):
        ext_transcript_grid.click_tab_by_name('Bad Tab Name')

def test_click_tab_by_name_raises_when_tabs_are_hidden(ext_transcript_grid):
    ext_transcript_grid.expand_tabs()
    with pytest.raises(AttributeError):
        ext_transcript_grid.click_tab_by_name('Detail')
