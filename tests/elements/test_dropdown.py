import pytest

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import Select
from selenium_mp.common.elements import get_element, Dropdown


@pytest.fixture(scope='class')
def dropdown1(driver):
    """A simple Input element without a prompt or a label."""
    URI = 'PROCESSMONITOR.PROCESSMONITOR.GBL'
    ENV = driver.env
    URL = f"https://{ENV}.dsc.umich.edu/psp/{ENV}op/EMPLOYEE/HRMS/c/{URI}"
    driver.get(URL)
    driver.switch_to_content()
    el = driver.find_element_by_id('PMN_FILTER_WRK_DISTSTATUS')
    return Dropdown(el)

testdata = [('', '  '),
            (6, 'Delete'),
            (3, 'Generated'),
            (1, 'N/A'),
            ('0', 'None')]

class TestDropdown:
    def test_label(self, dropdown1):
        assert dropdown1.label == 'Distribution Status'

    @pytest.mark.parametrize("value,expected", testdata)
    def test_set_value(self, value, expected, dropdown1):
        dropdown1.set_value(value)
        assert dropdown1.value == expected

    def test_dropdown_is_subclass_of_select(self, dropdown1):
        assert isinstance(dropdown1, Select)
