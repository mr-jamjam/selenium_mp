import pytest
from selenium.webdriver.common.by import By
from selenium_mp.common.elements import ScrollArea, Modal
from selenium_mp.common.exceptions import PeopleSoftError
from selenium_mp.common.pages import BaseComponentPage


class ConversionFieldScrollArea(ScrollArea):
    RECORD_LOCATORS = {
        'cnvr_fld': 'SCCFP_CNVR_FLD_SCCFP_CNVR_FIELD${idx}',
        'unmatched_handling': 'SCCFP_CNVR_FLD_SCCFP_CNVR_ERR${idx}',
        'default_value': 'SCCFP_CNVR_FLD_SCCFP_DFLT_VALUE${idx}',
        'fields': 'SCCFP_CNVR_VAL$scrolli${idx}'
    }


class FieldConversionPage(BaseComponentPage):
    COMPONENT_URI = 'SCCFP_MENU.SCCFP_CNVR.GBL'
    SEARCH_FIELDS = {
        'sccfp_fld_cnvr': (By.ID, 'SCCFP_CNVR_SCCFP_FLD_CNVR')
    }
    LOCATORS = {
        'name': (By.ID, 'SCCFP_CNVR_SCCFP_FLD_CNVR'),
        'long_descr': (By.ID, 'SCCFP_CNVR_DESCR254')
    }

    def __init__(self, driver):
        super().__init__(driver)
        driver.switch_to_content()
        self.conversions = ConversionFieldScrollArea(
            self.d,
            (By.ID, '$ICField17$scrolli$0'))


@pytest.fixture(scope='function')
def grid(driver):
    p = FieldConversionPage(driver)
    p.perform_search(sccfp_fld_cnvr='ZZ_TEST')
    fld = p.conversions.record(0)
    grid = fld.fields
    return grid

@pytest.fixture(scope='function')
def record(grid):
    r = grid.record(0)
    return r


class TestAddButton:

    def test_record_has_add_btn(self, record):
        assert hasattr(record, 'add_btn') == True

    def test_add_btn_id(self, record):
        expected = 'SCCFP_CNVR_VAL$new$0$$0'
        assert record.add_btn.get_attribute('id') == expected

    def test_click_add_btn(self, grid):
        initial = len(list(grid.records))
        grid.record(0).add_btn.click()
        final = len(list(grid.records))
        assert final > initial


class TestDeleteButton:

    def test_record_has_delete_btn(self, record):
        assert hasattr(record, 'delete_btn')

    def test_delete_btn_id(self, record):
        expected = 'SCCFP_CNVR_VAL$delete$0$$0'
        assert record.delete_btn.get_attribute('id') == expected

    def test_click_delete_btn(self, grid):
        initial = len(list(grid.records))
        try:
            grid.record(0).delete_btn.click()
        except PeopleSoftError:
            m = Modal(grid._el.parent)
            m.click_button_by_index(0)
        final = len(list(grid.records))
        assert final < initial
