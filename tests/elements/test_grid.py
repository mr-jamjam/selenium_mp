import pytest
from mp_driver import MPDriver
from selenium_mp.common.elements import Grid
from tests.config import *


URI = 'PROCESS_APPLICATIONS.ACAD_TST_RSLT_ADMA.GBL'
URL = f'https://{MP_ENV}.dsc.umich.edu/psc/{MP_ENV}op/EMPLOYEE/HRMS/c/{URI}'
ADM_APPL_NBR = '01880461'

@pytest.fixture(scope='module')
def grid():
    """Returns a test result grid instance."""
    d = MPDriver(MP_ENV)
    try:
        d.mp_login(MP_USERNAME, MP_PASSWORD)
        d.get(URL)
        d.find_element_by_id('ADM_APPL_SCTY_ADM_APPL_NBR').send_keys(ADM_APPL_NBR)
        d.find_element_by_id('#ICSearch').click()
        d.mp_wait()
    except:
        d.quit()
    else:
        yield Grid(d, ('id', 'STDNT_TEST_COMP$scrolli$0'))
        print('Tearing down MPDriver fixture.')
        d.quit()

def test_grid_title(grid):
    assert grid.title == 'Test Components'

def test_columns(grid):
    expected = ['Component', 'Score', 'Percentile', 'Test Date', 'Data Source',
                'Acad Level', 'Letter Score', 'Date Loaded', 'Index',
                'Stnd Admin']
    actual = list(grid.columns(normalize=False))
    assert actual == expected

def test_columns_normalized(grid):
    expected = ['component', 'score', 'percentile', 'test_date', 'data_source',
                'acad_level', 'letter_score', 'date_loaded', 'index',
                'stnd_admin']
    actual = list(grid.columns(normalize=True))
    assert actual == expected

def test_default_view_range(grid):
    assert grid.pagination.view_range == 4

def test_pagination_total(grid):
    assert grid.pagination.total == 12

def test_pagination_default_first_in_view(grid):
    assert grid.pagination.first_in_view == 1

def test_pagination_default_last_in_view(grid):
    assert grid.pagination.last_in_view == 4

def test_grid_counter(grid):
    assert grid.grid_counter == '1-4 of 12'

def test_view_all(grid):
    assert grid.pagination.view_range == 4
    grid.view_all()
    assert grid.pagination.view_range == 12

def test_toggle_view(grid):
    initial = grid.pagination.view_range
    grid.toggle_view()
    final = grid.pagination.view_range
    assert initial != final

def test_next_record(grid):
    if grid.pagination.view_range == grid.pagination.total:
        grid.toggle_view()
    initial = grid.pagination.first_in_view
    expected = initial + grid.pagination.view_range
    grid.next_record()
    final = grid.pagination.first_in_view
    assert final == expected

def test_last_record(grid):
    grid.last_record()
    assert grid.pagination.last_in_view == grid.pagination.total

def test_first_record(grid):
    grid.first_record()
    assert grid.pagination.first_in_view == 1
