import pytest

from selenium.webdriver.common.by import By
from selenium_mp.common import elements
from selenium_mp.common.elements import ScrollArea
from selenium_mp.common.pages import BaseComponentPage


class FieldConversionPage(BaseComponentPage):
    COMPONENT_URI = 'SCCFP_MENU.SCCFP_CNVR.GBL'
    SEARCH_FIELDS = {
        'sccfp_fld_cnvr': (By.ID, 'SCCFP_CNVR_SCCFP_FLD_CNVR')
    }
    LOCATORS = {
        'name': (By.ID, 'SCCFP_CNVR_SCCFP_FLD_CNVR'),
        'long_descr': (By.ID, 'SCCFP_CNVR_DESCR254')
    }

    def __init__(self, driver):
        super().__init__(driver)
        driver.switch_to_content()
        self.conversions = ConversionFieldScrollArea(
            self.d,
            (By.ID, '$ICField17$scrolli$0'))


class ConversionFieldScrollArea(ScrollArea):
    RECORD_LOCATORS = {
        'cnvr_fld': 'SCCFP_CNVR_FLD_SCCFP_CNVR_FIELD${idx}',
        'unmatched_handling': 'SCCFP_CNVR_FLD_SCCFP_CNVR_ERR${idx}',
        'default_value': 'SCCFP_CNVR_FLD_SCCFP_DFLT_VALUE${idx}',
        'fields': 'SCCFP_CNVR_VAL$scrolli${idx}'
    }


element_data = [('cnvr_fld', elements.Input),
                ('unmatched_handling', elements.Dropdown),
                ('default_value', elements.Input),
                ('fields', elements.Grid)]


@pytest.fixture(scope='function')
def scrollarea(driver):
    p = FieldConversionPage(driver)
    p.perform_search(sccfp_fld_cnvr='ZZ_TEST')
    return p.conversions

@pytest.fixture(scope='function')
def record1(scrollarea):
    return scrollarea.record(0)

class TestFieldConversionScrollArea:

    def test_is_scrollarea_instance(self, scrollarea):
        assert isinstance(scrollarea, ScrollArea)

    def test_scrollarea_init_index(self, scrollarea):
        assert scrollarea._init_record_idx() == 0

    @pytest.mark.parametrize('name,element', element_data)
    def test_elements_parsed_correctly(self, name, element, record1):
        attr = getattr(record1, name)
        assert isinstance(attr, element)

    @pytest.mark.parametrize('attr',
                             ConversionFieldScrollArea.RECORD_LOCATORS.keys())
    def test_record1_has_attrs_from_record_locators(self, attr, record1):
        assert hasattr(record1, attr)

    def test_record1_conversion_field_value(self, record1):
        assert record1.cnvr_fld.value == 'test'
