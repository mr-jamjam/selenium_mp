import pytest

from selenium.webdriver.common.by import By
from selenium_mp.common import elements
from selenium_mp.common.elements import get_element
from selenium_mp.common.utils import get_uri


ADM_APPL_NBR = '00795039'
test_data = [
    ('EXT_ACAD_DATA_SAD_DISMISSED$0', elements.CheckBox),
    ('M_RA_TRANSCRIPT_RECEIVED_FLAG$0', elements.CheckBox),
    ('EXT_ACAD_DATA_EXT_TERM_TYPE$0', elements.Dropdown),
    ('EXT_ACAD_DATA_EXT_DATA_NBR$0', elements.Element),
    ('M_RA_TRANSCRIPT$scrolli$0', elements.Grid),
    ('DERIVED_ADM$scrolli$0', elements.Grid),
    ('M_RA_TRANSCRIPT_TRANSCRIPT_DT$0', elements.Input),
    ('M_RA_EXT_COMMNT_COMMENTS_2000$0', elements.Input)]


@pytest.fixture(scope='module')
def educ_page(driver):
    uri = 'PROCESS_APPLICATIONS.ACAD_HISTORY_ADMA.GBL'
    get_uri(driver, uri)
    appl_nbr = driver.find_element_by_id('ADM_APPL_SCTY_ADM_APPL_NBR')
    appl_nbr.clear()
    appl_nbr.send_keys(ADM_APPL_NBR)
    emplid = driver.find_element_by_id('ADM_APPL_SCTY_EMPLID')
    emplid.clear()
    driver.find_element_by_id('#ICSearch').click()
    driver.mp_wait()
    driver.switch_to_content()
    return driver

@pytest.mark.parametrize('idx,element', test_data)
def test_elements_returned(idx, element, educ_page):
    loc = (By.ID, idx)
    el = get_element(educ_page, loc)
    assert isinstance(el, element)
