import pytest
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException

from selenium_mp.common.elements import CheckBox
from selenium_mp.common.exceptions import CheckBoxDisabledError
from selenium_mp.common.utils import get_uri


@pytest.fixture(scope='class')
def checkbox(driver):
    get_uri(driver, 'PROCESSMONITOR.PROCESSMONITOR.GBL')
    el = driver.find_element_by_id('PMN_FILTER_WRK_PMN_SAVE_FLAG')
    return CheckBox(el)

@pytest.fixture(scope='class')
def disabled_checkbox(driver):
    get_uri(driver, 'PROCESSMONITOR.PROCESSMONITOR.GBL')
    el = driver.find_element_by_id('SELECT_FLAG$0')
    return CheckBox(el)


class TestEnabledCheckBox:


    def test_checked(self, checkbox):
        assert checkbox.checked == True

    def test_is_not_disabled(self, checkbox):
        assert checkbox.is_disabled == False

    def test_label(self, checkbox):
        assert checkbox.label == 'Save On Refresh'

    def test_value(self, checkbox):
        checkbox.check()
        assert checkbox.value == True
        checkbox.uncheck()
        assert checkbox.value == False

    def test_check(self, checkbox):
        checkbox.check()
        assert checkbox.checked == True

    def test_uncheck(self, checkbox):
        checkbox.uncheck()
        assert checkbox.checked == False

    def test_toggle(self, checkbox):
        initial = checkbox.checked
        checkbox.toggle()
        final = checkbox.checked
        assert initial != final


class TestDisabledCheckbox:

    def test_label(self, disabled_checkbox):
        with pytest.raises(NoSuchElementException):
            disabled_checkbox.label

    def test_is_disabled(self, disabled_checkbox):
        assert disabled_checkbox.is_disabled == True

    def test_check_raises_exception(self, disabled_checkbox):
        with pytest.raises(CheckBoxDisabledError):
            disabled_checkbox.check()

    def test_uncheck_raises_exception(self, disabled_checkbox):
        with pytest.raises(CheckBoxDisabledError):
            disabled_checkbox.uncheck()

    def test_toggle_raises_exception(self, disabled_checkbox):
        with pytest.raises(CheckBoxDisabledError):
            disabled_checkbox.toggle()
