import pytest
from selenium.common.exceptions import NoSuchElementException
from selenium_mp.common.elements import get_element, Input, Prompt
from selenium_mp.common.utils import get_uri


@pytest.fixture(scope='class')
def input1(driver):
    """A simple Input element without a prompt or a label."""
    URI = 'SCCFP_MENU.SCCFP_CNVR.GBL'
    get_uri(driver, URI)
    driver.switch_to_content()
    input = get_element(driver, ('id', 'SCCFP_CNVR_SCCFP_FLD_CNVR'))
    return input


@pytest.fixture(scope='class')
def input2(driver):
    """An input with a lookup prompt and a label."""
    URI = 'SCCFP_MENU.SCCFP_CNVR.GBL'
    get_uri(driver, URI)
    driver.switch_to_content()
    driver.find_element_by_id('SCCFP_CNVR_SCCFP_FLD_CNVR').send_keys('M_RA_CAP')
    driver.find_element_by_id('#ICSearch').click()
    driver.mp_wait()
    input = get_element(driver, ('id', 'SCCFP_CNVR_FLD_RECNAME$0'))
    return input


class TestInput1:
    def test_input_label(self, input1):
        """
        An input without a label should raise a NoSuchElementException when
        accessing the label property.
        """
        with pytest.raises(NoSuchElementException):
            label = input1.label

    def test_has_prompt(self, input1):
        assert input1.has_prompt == False

    def test_open_prompt_fails(self, input1):
        with pytest.raises(NoSuchElementException):
            input1.open_prompt()

    def test_value(self, input1):
        assert input1.value == ''
        input1.set_value('test value')
        assert input1.value == 'test value'


@pytest.mark.usefixtures('input2')
class TestInput2:
    def test_input_label(self, input2):
        assert input2.label == 'Record (Table) Name'

    def test_has_prompt(self, input2):
        assert input2.has_prompt == True

    def test_open_prompt(self, request, input2):
        prompt = input2.open_prompt()
        assert isinstance(prompt, Prompt) == True
        prompt.close()

    def test_value(self, input2):
        assert input2.value == ''
        for value in ['test', 2, 1.2, True, None]:
            input2.set_value(value)
            assert input2.value == str(value)
