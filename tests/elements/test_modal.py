import pytest
from selenium.common.exceptions import NoSuchElementException

from selenium_mp.common.elements import Modal


def get_uri(driver, uri):
    env = driver.env
    url = f"https://{env}.dsc.umich.edu/psp/{env}op/EMPLOYEE/SA/c/{uri}"
    driver.get(url)


@pytest.fixture(scope='function')
def modal(driver):
    emplid = '48766114'
    uri = "PROCESS_APPLICATIONS.ACAD_HISTORY_ADMA.GBL"
    get_uri(driver, uri)
    driver.switch_to_content()
    emplid_el = driver.find_element_by_id('ADM_APPL_SCTY_EMPLID')
    emplid_el.clear()
    emplid_el.send_keys(emplid)
    driver.find_element_by_id('#ICSearch').click()
    driver.mp_wait()
    driver.find_element_by_id('ACAD_HISTORY_EXT_ORG_ID$0').clear()
    driver.find_element_by_id('#ICSave').click()
    driver.mp_wait()
    return Modal(driver)


class TestModal:
    def test_title(self, modal):
        assert modal.title == 'Message'

    def test_message(self, modal):
        expected = ("Field is required. (15,8)\n\n"
                    "You have left a field empty and a value must be entered.")
        assert modal.message == expected

    def test_role(self, modal):
        assert modal.role == 'alertdialog'

    def test_buttons(self, modal):
        expected = ['OK']
        actual = [button.get_attribute('value') for button in modal.buttons]
        assert actual == expected

    def test_click_button_by_text(self, modal, driver):
        modal.click_button_by_text('OK')
        pt_modalMask = driver.find_element_by_id('pt_modalMask')
        assert pt_modalMask.is_displayed() == False

    def test_click_button_by_text_with_invalid_text(self, modal):
        with pytest.raises(NoSuchElementException):
            modal.click_button_by_text('Not a button')

    def test_click_button_by_index(self, modal, driver):
        modal.click_button_by_index(0)
        pt_modalMask = driver.find_element_by_id('pt_modalMask')
        assert pt_modalMask.is_displayed() == False

    def test_click_button_by_index_invalid_index(self, modal):
        with pytest.raises(NoSuchElementException):
            modal.click_button_by_index(1)
