"""
Example page object for an MPathways Homepage.
"""
from collections import namedtuple
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from selenium_mp.common.pages import BasePage
from selenium_mp.common.elements import get_element


class HomePage(BasePage):
    """Example MPathways HomePage object."""
    COMPONENT_URI = 'NUI_FRAMEWORK.PT_LANDINGPAGE.GBL'
    PAGE = 'PT_LANDINGPAGE'
    CLASS_LOCATORS = {
        'homepage_dropdown': (By.XPATH, "//a[@aria-label='Choose homepage']"),
        'homepage_options': (By.CSS_SELECTOR, "li.ps_menuitem"),
        'tiles': (By.CSS_SELECTOR, "div.nuitile")
    }
    LOCATORS = {
        'home_btn': (By.ID, 'PT_HOME')
    }

    @property
    def homepage(self):
        """
        The current homepage for the user. Assignment to this property will
        change the homepage.
        """
        return self._get_element('homepage_dropdown').text.strip()

    @homepage.setter
    def homepage(self, value):
        self._get_element('homepage_dropdown').click()
        option_loc = (By.LINK_TEXT, value)
        wait = WebDriverWait(self.d, 5)
        try:
            option = wait.until(EC.element_to_be_clickable(option_loc))
        except:
            raise ValueError(f'Invalid homepage: {value}.')
        else:
            option.click()

    @property
    def homepage_options(self):
        """
        Return a list of homepage options available to the user.
        """
        try:
            self._get_element('homepage_dropdown').click()
        except:
            pass
        else:
            return [option.text for option
                in self._get_elements('homepage_options')
                if option.text.strip() != '']
        finally:
            self._get_element((By.ID, 'pt_modalMask')).click()

    @property
    def tiles(self):
        """
        Return a generator yielding all tile elements on the page.
        """
        Tile = namedtuple('Tile', ['label', 'row', 'col', 'element'])
        loc = (By.CSS_SELECTOR, "div.nuitile")
        for tile in self.d.find_elements(*loc):
            # verify that tile is not a hidden tile
            label = tile.find_element_by_tag_name('h2').text.strip()
            if label:
                col = int(tile.get_attribute('tx'))
                row = int(tile.get_attribute('ty'))
                yield Tile(label=label, row=row, col=col, element=tile)
