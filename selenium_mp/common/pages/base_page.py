from collections import namedtuple
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium_mp.common.elements import get_element


class BasePage(object):
    COMPONENT_URI = ''
    COMPONENT = ''
    PAGE = ''
    CLASS_LOCATORS = {}
    LOCATORS = {}

    def __init__(self, driver):
        self.d = driver
        self.d.switch_to_content()

    def __dir__(self):
        return sorted(super().__dir__() + list(self.LOCATORS.keys()))

    def __getattr__(self, name):
        loc = self.LOCATORS.get(name)
        if loc:
            try:
                el = get_element(self.d, loc)
                return el
            except TimeoutException:
                raise NoSuchElementException(
                    f'Could not locate element for locator: {loc}.')
        else:
            raise AttributeError

    @property
    def base_url(self):
        env = self.env
        return f"https://{env}.dsc.umich.edu/psc/{env}op/EMPLOYEE/HRMS/c/"

    @property
    def env(self):
        env, _ = self.env_info['db'].split('/')
        return env.lower()

    @property
    def env_info(self):
        """
        Return EnvInfo named tuple containing environment information.
        """
        locator = (By.XPATH, '//div[starts-with(@id, "pt_envinfo")]')
        wait = WebDriverWait(self.d, 10)
        el = wait.until(EC.presence_of_element_located(locator))
        attribs = {'devicetype': None,
                   'browser': None,
                   'toolsrel': None,
                   'user': None,
                   'db': None,
                   'appserv': None}
        for key in attribs.keys():
            attribs[key] = el.get_attribute(key)
        EnvInfo = namedtuple('EnvInfo', attribs.keys())
        return EnvInfo(**attribs)

    @property
    def modal_present(self):
        """
        Returns True when a modal is present on the page.
        """
        try:
            self.d.find_element_by_class_name('PSMODALTABLE')
        except NoSuchElementException:
            return False
        else:
            return True

    @property
    def page_info(self):
        """
        Return PageInfo named tuple containing page information.
        """
        self.d.switch_to_content()
        locator = (By.XPATH, '//div[starts-with(@id, "pt_pageinfo")]')
        wait = WebDriverWait(self.d, 10)
        el = wait.until(EC.presence_of_element_located(locator))
        attribs = {'page': None,
                   'component': None,
                   'menu': None,
                   'mode': None}
        for key in attribs.keys():
            attribs[key] = el.get_attribute(key)
        PageInfo = namedtuple('PageInfo', attribs.keys())
        return PageInfo(**attribs)

    @property
    def window(self):
        """
        Return the PS window handle for the current page ('win0').
        """
        xpath = "//div[starts-with(@id, 'pt_envinfo')]"
        el = self.d.find_element_by_xpath(xpath)
        *_, window = el.get_attribute('id').split('_')
        return window

    def _clear_modal(self):
        xpath = """//div[@id='pt_modals']//input[@id='#ICOK']"""
        el = d.find_element_by_xpath(xpath)
        el.click()

    def _get_element(self, locator):
        """
        Return an element for the locator provided.

        If `locator` is a string, this method will search class.LOCATORS and
        class.CLASS_LOCATORS dicts for a matching key. Otherwise the method
        will assume the locator is a tuple [(By.ID, 'some-id')] and send
        `locator` to driver.find_element(*locator).
        """
        self.d.switch_to_content()
        if isinstance(locator, str):
            loc = self.LOCATORS.get(locator) or self.CLASS_LOCATORS.get(locator)
        else:
            loc = locator
        return self.d.find_element(*loc)

    def _get_elements(self, locator):
        """
        Return a list of elements for the locator provided.

        If `locator` is a string, this method will search class.LOCATORS and
        class.CLASS_LOCATORS dicts for a matching key. Otherwise the method
        will assume the locator is a tuple [(By.ID, 'some-id')] and send
        `locator` to driver.find_elements(*locator).
        """
        self.d.switch_to_content()
        if isinstance(locator, str):
            loc = self.LOCATORS.get(locator) or self.CLASS_LOCATORS.get(locator)
        else:
            loc = locator
        return self.d.find_elements(*loc)

    def _get_element_value(self, locator):
        """Return the value of an element.

        If element is an input, the 'value' attribute is returned,
        otherwise the text value is returned."""
        el = self._get_element(locator)
        if el.tag_name == 'input':
            return el.get_attribute('value')
        else:
            return el.text.strip()

    def _set_element_value(self, locator, value, clear=True):
        """Set an input element value"""
        el = self._get_element(locator)
        if el.tag_name == 'input':
            if clear:
                el.clear()
            el.send_keys(value + Keys.TAB)
            self.d.mp_wait(30)
        else:
            raise AttributeError(f'Cannot set value for {el.tag_name} element.')

    def get(self):
        """
        Load the COMPONENT_URI and return an instance of the class.

        Parameters
        ----------
        driver : MPDriver
            An MPDriver instance with an active MPathways session.
        """
        uri = self.COMPONENT_URI
        env = self.d.env
        url = f"https://{env}.dsc.umich.edu/psc/{env}op/EMPLOYEE/HRMS/c/{uri}"
        self.d.get(url)
        self.d.mp_wait()
        return self
