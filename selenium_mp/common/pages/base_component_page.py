from .base_page import BasePage
from .base_search_page import BaseSearchPage


class BaseComponentPage(BasePage):
    SEARCH_FIELDS = {}

    def perform_search(self, **kwargs):
        self.get()  # get search page
        p = BaseSearchPage(self.d, locators=self.SEARCH_FIELDS)
        for k, v in kwargs.items():
            getattr(p, k).set_value(str(v))
        if kwargs:
            p.search()
        if p.page_info.page == '(search)':
            return p
        else:
            return self
