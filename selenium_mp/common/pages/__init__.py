from .base_page import BasePage
from .base_search_page import BaseSearchPage
from .base_component_page import BaseComponentPage
