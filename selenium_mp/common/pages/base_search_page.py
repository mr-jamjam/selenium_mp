from collections import namedtuple
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium_mp.common.utils import normalize_string
from .base_page import BasePage


class BaseSearchPage(BasePage):
    PAGE = '(search)'
    LOCATORS = {}
    CLASS_LOCATORS = {
        'clear_btn': (By.ID, '#ICClear'),
        'search_btn':(By.XPATH, '//input[@id="#ICSearch" and @value="Search"]'),
        'search_results_tbl': (By.ID, 'PTSRCHRESULTS'),
        'search_results_hdr': (By.CSS_SELECTOR, 'h2.PSSRCHSUBTITLE')}

    def __init__(self, driver, **kwargs):
        if kwargs.get('locators'):
            for k, v in kwargs.get('locators').items():
                self.LOCATORS[k] = v
        super().__init__(driver)

    @property
    def has_search_results(self):
        """
        Indicator of whether search results are present on the page.
        """
        try:
            el = self._get_element('search_results_hdr')
        except Exception as e:
            return False
        else:
            return el.text == 'Search Results'

    @property
    def search_results(self):
        """
        Return a generator of SearchResult named tuples.

        Each SearchResult will have the same properties as the search results
        grid on the page.
        """
        self.d.switch_to_content()
        try:
            result_tbl = self._get_element('search_results_tbl')
        except NoSuchElementException:
            return
        hdr, *results = result_tbl.find_elements_by_tag_name('tr')
        headers = [normalize_string(th.text)
                   for th in hdr.find_elements_by_tag_name('th')]
        SearchResult = namedtuple('SearchResult', headers)
        for result in results:
            values = [td.text for td in result.find_elements_by_tag_name('td')]
            yield SearchResult(*values)

    def clear(self):
        """
        Click the 'Clear' button, resetting the fields on the search page.
        """
        self.d.switch_to_content()
        self._get_element('clear_btn').click()
        self.d.mp_wait()

    def get_search_result(self, result_index):
        """
        Click the search result at position `index`, with the first result
        being index 0.

        Returns
        -------
        None

        Raises
        ------
        AttributeError :
            Raised when no search results present on page.
        """
        self.d.switch_to_content
        if not self.has_search_results:
            raise AttributeError('No search results on page.')
        result_tbl = self._get_element('search_results_tbl')
        _, *results = result_tbl.find_elements_by_tag_name('tr')
        link = results[result_index].find_element_by_tag_name('a')
        link.click()
        self.d.mp_wait(180)

    def search(self):
        """
        Click the 'Search' button.

        This will yield the following outcomes:
            - If the search matches a single result, that result will be
              loaded directly.
            - If the search matches multiple results, the search page will be
              updated with a grid of search results.
            - If no results are found, the page will be updated with a message
              stating 'No results found.'
            - If there is an issue with the search, an error box will appear
              describing the error.
        """
        self._get_element('search_btn').click()
        self.d.mp_wait(180)
