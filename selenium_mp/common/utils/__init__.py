import re
from functools import wraps

from selenium_mp.common.exceptions import PeopleSoftError


def handle_alert(func):
    """
    Decorator to handle alerts for page objects. Raises a PeopleSoftError with
    the alert text if an alert is present after the function has executed.
    """
    @wraps(func)
    def func_wrapper(*args, **kwargs):
        r = func(*args, **kwargs)
        self = args[0]
        self.d.mp_wait()
        try:
            alert = self.d.find_element_by_id('alertmsg')
        except:
            return r
        else:
            raise PeopleSoftError(alert.text)
    return func_wrapper

def normalize_string(text):
    """
    Normalize a string by applying lowercase and replacing non-alpha with '_'.
    """
    sub = re.sub(r"[^a-z]+", '_', text.lower())
    return sub.lstrip('_').rstrip('_')

def apply_style(webelement, style):
    """
    Apply an inline style to the webelement.

    Parameters
    ----------
    webelement : webelement
        A selenium webelement.
    style : str
        The inline styling to apply, e.g.:
        'background: yellow; border: 2px solid red;'
    """
    webelement.parent.execute_script(
        f"arguments[0].setAttribute('style', '{style}');",
        webelement)

def get_uri(driver, uri):
    """
    Load a specific PeopleSoft URI (content_id).

    This function assumes that the driver is currently in a PS environment.
    """
    pattern = (r"(?P<protocol>.+)://(?P<server>.+)/(?P<servlet>.+)/"
               r"(?P<sitename>.+)/(?P<portal>.+)/(?P<node>.+)/"
               r"(?P<content_type>.+)/(?P<content>.+)")
    current_url = driver.current_url
    try:
        url_parts = re.match(pattern, current_url).groupdict()
    except AttributeError:
        raise AttributeError(f'Unknown URL format: {current_url}')
    url = ("{protocol}://{server}/{servlet}/{sitename}/{portal}/{node}/"
           "{content_type}/{uri}")
    driver.get(url.format(**url_parts, uri=uri))
    driver.switch_to_content()
