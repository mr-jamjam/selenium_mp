from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select


class Dropdown(Select):
    """
    Standard model for <select> elements.
    """
    @property
    def label(self):
        """Return the text of the <label> associated with the element."""
        element_id = self._el.get_attribute('id')
        d = self._el.parent
        label = d.find_element_by_xpath(f"//label[@for = '{element_id}']")
        return label.text

    @property
    def value(self):
        """Return the text of the first selected option."""
        return self.first_selected_option.text

    def set_value(self, value):
        """Select the appropriate dropdown option.

        Parameters
        ----------
        value : str
            Either the value or text of the option to select.

        Raises
        ------
        NoSuchElementException
            Raised when an option cannot be selected by value or by
            visible text.
        """
        try:
            self.select_by_value(str(value))
        except NoSuchElementException:
            self.select_by_visible_text(str(value))
