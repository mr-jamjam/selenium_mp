from selenium.webdriver.common.by import By
from selenium.common.exceptions import UnexpectedTagNameException
from selenium_mp.common.exceptions import CheckBoxDisabledError
from .base_element import BaseElement


class CheckBox(BaseElement):

    def __init__(self, webelement):
        """
        Constructor. A check is made that the given element is, indeed, a
        checkbox element. If it is not, then an UnexpectedTagNameException
        is thrown.
        """
        if webelement.tag_name.lower() != 'input':
            if webelement.get_attribute('type') != 'checkbox':
                raise UnexpectedTagNameException(
                    "CheckBox only works on <input type='checkbox'> elements.")
        super().__init__(webelement)

    @property
    def checked(self):
        """Return True if the checkbox is checked."""
        if not self.is_enabled():
            return self._get_element().get_attribute('checked') == 'true'
        else:
            sibling_xpath = "./preceding-sibling::input"
            value_el = self._get_element().find_element_by_xpath(sibling_xpath)
            return value_el.get_attribute('value').lower() == 'y'

    @property
    def is_disabled(self):
        """Return True if the checkbox is disabled."""
        return not self._get_element().is_enabled()

    @property
    def label(self):
        """Return the text of the <label> associated with the element."""
        el = self.d.find_element_by_xpath(f"//label[@for = '{self._id}']")
        return el.text

    @property
    def value(self):
        """
        Return True if checkbox is checked.
        Convenience property wrapping CheckBox.checked.
        """
        return self.checked

    def set_value(self, value):
        if value:
            self.check()
        else:
            self.uncheck()

    def toggle(self):
        """Toggle the checkbox."""
        if self.is_disabled:
            raise CheckBoxDisabledError('This checkbox is disabled.')
        self._get_element().click()
        self.d.mp_wait()

    def check(self):
        """Check the checkbox."""
        if self.is_disabled:
            raise CheckBoxDisabledError('This checkbox is disabled.')
        if not self.checked:
            self.toggle()

    def uncheck(self):
        """Uncheck the checkbox."""
        if self.is_disabled:
            raise CheckBoxDisabledError('This checkbox is disabled.')
        if self.checked:
            self.toggle()
