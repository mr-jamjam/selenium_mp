from selenium.common.exceptions import (NoSuchElementException,
                                        UnexpectedTagNameException)
from selenium.webdriver.support.ui import Select
from .base_element import BaseElement
from .prompt import Prompt


class Input(BaseElement):
    """
    Standard <input> element model.
    """
    def __init__(self, webelement):
        super().__init__(webelement)
        if webelement.tag_name.lower() not in ['input', 'textarea']:
            raise UnexpectedTagnameException(
                "Input only works on <input> elements.")

    @property
    def has_prompt(self):
        """
        Return True when there is a prompt associated with the input element.
        """
        xpath = "./following-sibling::a[contains(@id, 'prompt')]"
        try:
            self._get_element().find_element_by_xpath(xpath)
        except NoSuchElementException:
            return False
        else:
            return True

    @property
    def label(self):
        """Return the text of the <label> associated with the element."""
        label = self.find_element_by_xpath(f"//label[@for = '{self._id}']")
        return label.text

    @property
    def value(self):
        el = self._get_element()
        return el.get_attribute('value')

    def open_prompt(self):
        xpath = "./following-sibling::a[contains(@id, 'prompt')]"
        self._get_element().find_element_by_xpath(xpath).click()
        self.d.mp_wait()
        return Prompt(self.d)

    def set_value(self, value):
        el = self._get_element()
        el.clear()
        el.send_keys(str(value))
        self.d.mp_wait()
