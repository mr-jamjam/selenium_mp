from selenium.common.exceptions import NoSuchElementException
from .base_element import BaseElement


class Element(BaseElement):

    @property
    def label(self):
        """Return the text of the <label> associated with the element."""
        try:
            el = self.d.find_element_by_xpath(f"//label[@for = '{self._id}']")
            return el.text
        except NoSuchElementException:
            pass
        try:
            el = self.d.find_element_by_xpath(f"//*[contains(@id, '{self._id}lbl')]")
            return el.text
        except NoSuchElementException:
            raise

    @property
    def value(self):
        return self._get_element().text
