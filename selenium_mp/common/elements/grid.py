from collections import namedtuple
from selenium.webdriver.common.by import By
from selenium_mp.common.elements import get_element, Element
from selenium_mp.common.utils import normalize_string


class Grid(object):

    def __init__(self, driver, locator):
        self.d = driver
        self._loc = locator

    @property
    def _el(self):
        return self.d.find_element(*self._loc)

    @property
    def has_add_delete(self):
        """
        Return True when the add/delete buttons are visible.
        """
        try:
            self._el.find_element_by_css_selector('a.PTROWADD1')
            return True
        except:
            return False

    @property
    def title(self):
        xpath = (".//td[contains(@class, 'GRIDLABEL')]"
                 "//td[contains(@class, 'GRIDLABEL')]")
        return self._el.find_element_by_xpath(xpath).text.strip()

    def columns(self, normalize=False):
        """
        List of column names currently displayed by the grid.

        Parameters:
        normalize : boolean
            Flag controlling whether to normalize the column names by converting
            to lowercase and replacing any non-alpha characters with '_'.
        """
        xpath = ".//th[contains(@class, 'GRIDCOLUMNHDR')]/a"
        els = self._el.find_elements_by_xpath(xpath)
        if normalize:
            cols = [normalize_string(el.text.strip()) for el in els]
        else:
            cols = [el.text.strip() for el in els]
        if self.has_add_delete:
            return cols + ['add_btn', 'delete_btn']
        else:
            return cols

    @property
    def records(self):
        """
        A generator of Records currently displayed by the grid.

        Each Record is a namedtuple with attributes matching the column
        headings that are currently displayed. Each attribute will be a
        corresponding selenium_mp element object.
        """
        columns = list(self.columns(normalize=True))
        Record = namedtuple('Record', columns)
        classes = ['.PSCHECKBOX',
                   '.PSDROPDOWN',
                   '.PSDROPDOWNLIST',
                   '.PSDROPDOWNLIST_DISPONLY',
                   '.PSEDITBOX',
                   '.PSEDITBOX_DISPONLY',
                   '.PSHYPERLINK',
                   '.PSHYPERLINKDISABLED',
                   '.PTROWADD1',
                   '.PTROWDELETE1']
        tr_xpath = ".//tr[contains(@id, '_row')]"
        for tr in self._el.find_elements_by_xpath(tr_xpath):
            elements = []
            for td in tr.find_elements_by_tag_name('td')[1:]:
                # first occurrence in cell of any PS element classes
                # this will avoid prompts, etc. that we don't want.
                try:
                    el = td.find_element_by_css_selector(','.join(classes))
                except:
                    elements.append(Element(td))  # td contains nothing of interest
                else:
                    el_id = el.get_attribute('id')
                    elements.append(get_element(self.d, (By.ID, el_id)))
            kwargs = dict(zip(columns, elements))
            yield Record(**kwargs)

    def record(self, idx):
        """
        Access the record at `idx` within the grid. The first record is
        at `idx` 0.
        """
        columns = list(self.columns(normalize=True))
        classes = ['.PSCHECKBOX',
                   '.PSDROPDOWN',
                   '.PSDROPDOWNLIST',
                   '.PSDROPDOWNLIST_DISPONLY',
                   '.PSEDITBOX',
                   '.PSEDITBOX_DISPONLY',
                   '.PSHYPERLINK',
                   '.PSHYPERLINKDISABLED',
                   '.PTROWADD1',
                   '.PTROWDELETE1']
        tr_xpath = f"(.//tr[contains(@id, '_row')])[{idx+1}]"
        tr = self._el.find_element_by_xpath(tr_xpath)
        elements = []
        for td in tr.find_elements_by_tag_name('td')[1:]:
            try:
                el = td.find_element_by_css_selector(','.join(classes))
            except:
                elements.append(Element(td))
            else:
                if 'PTROWADD1' in el.get_attribute('class'):
                    columns.append('add_btn')
                elif 'PTROWDELETE1' in el.get_attribute('class'):
                    columns.append('delete_btn')
                el_idx = el.get_attribute('id')
                elements.append(get_element(self.d, (By.ID, el_idx)))
        Record = namedtuple('Record', columns)
        kwargs = dict(zip(columns, elements))
        return Record(**kwargs)

    def first_record(self):
        xpath = ".//td[@class='PSGRIDNAVIGATOR']//a[contains(@id, 'htop')]"
        self._el.find_element_by_xpath(xpath).click()
        self.d.mp_wait()

    def prev_record(self):
        xpath = ".//td[@class='PSGRIDNAVIGATOR']//a[contains(@id, 'hup')]"
        self._el.find_element_by_xpath(xpath).click()
        self.d.mp_wait()

    def next_record(self):
        xpath = ".//td[@class='PSGRIDNAVIGATOR']//a[contains(@id, 'hdown')]"
        self._el.find_element_by_xpath(xpath).click()
        self.d.mp_wait()

    def last_record(self):
        xpath = ".//td[@class='PSGRIDNAVIGATOR']//a[contains(@id, 'hend')]"
        self._el.find_element_by_xpath(xpath).click()
        self.d.mp_wait()

    def view_all(self):
        xpath = (".//td[@class='PSGRIDNAVIGATOR']"
                 "//a[contains(@id, 'hviewall') and text() = 'View All']")
        self._el.find_element_by_xpath(xpath).click()
        self.d.mp_wait()

    def toggle_view(self):
        xpath = ".//td[@class='PSGRIDNAVIGATOR']//a[contains(@id, 'hviewall')]"
        self._el.find_element_by_xpath(xpath).click()
        self.d.mp_wait()

    def expand_tabs(self):
        """Show all tabs."""
        xpath = ".//a[@name='hidetabs'][//img[@title='Show all columns']]"
        self._el.find_element_by_xpath(xpath).click()
        self.d.mp_wait()

    def collapse_tabs(self):
        """Collapse tabs."""
        xpath = ".//a[@name='hidetabs'][//img[@title='Show tabs']]"
        self._el.find_element_by_xpath(xpath).click()
        self.d.mp_wait()

    def toggle_tabs(self):
        """Toggle tabs."""
        xpath = ".//a[@name='hidetabs']"
        self._el.find_element_by_xpath(xpath).click()
        self.d.mp_wait()

    def click_tab_by_name(self, tabname):
        """Click tab for `tabname`."""
        tab_names = self.tab_names
        if len(tab_names) == 0:
            raise AttributeError('No tabs are currently displayed.')
        elif tabname not in tab_names:
            raise ValueError(f'{tabname} not in {tab_names}.')
        idx = tab_names.index(tabname)
        self.tabs[idx].click()
        self.d.mp_wait()

    @property
    def tabs(self):
        """Webelements for tabs currently displayed by the grid."""
        xpath = ".//div[@class='PTGRIDTAB']//a[@role='tab']"
        return self._el.find_elements_by_xpath(xpath)

    @property
    def tab_names(self):
        """Names of tabs currently displayed by the grid."""
        return [tab.text.strip() for tab in self.tabs]

    @property
    def grid_counter(self):
        """
        Return the text of the PSGRIDCOUNTER element, which indicates the
        current index displayed by the grid ('1-5 of 10').
        """
        css = 'span.PSGRIDCOUNTER'
        return self._el.find_element_by_css_selector(css).text

    @property
    def pagination(self):
        """
        Returned a Pagination named tuple.

        Pagination attributes
        ---------------------
        first : int
            index of first record in view.
        last : int
            index of the last record in view.
        total : int
            total records in the scroll area.
        view_range : int
            total records displayed on current page.
        """
        Pagination = namedtuple('Pagination', ['first_in_view',
                                               'last_in_view',
                                               'total',
                                               'view_range'])
        view, _, total = self.grid_counter.split(' ')
        if '-' in view:
            first, last = [int(x) for x in view.split('-')]
        else:
            first = int(view)
            last = first
        view_range = last - (first - 1)
        total = int(total)
        return Pagination(first_in_view=first,
                          last_in_view=last,
                          total=total,
                          view_range=view_range)
