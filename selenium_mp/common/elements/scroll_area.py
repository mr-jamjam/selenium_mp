import re
from collections import namedtuple
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium_mp.common.elements.get_element import get_element


class ScrollArea(object):
    """
    A level 1 scroll area object.

    Any subclasses should set a static RECORD_LOCATORS dict containing
    {property_name: element_id} for any repeating items on the page:

    RECORD_LOCATORS = {
        'propery1': 'PROPERTY_ID${idx}',
        'property2': 'PROPERTY2_ID$$0$scroll${idx}'
    }
    """
    LOCATOR = ''
    RECORD_LOCATORS = {}  # dict of property names, ids

    def __init__(self, driver, locator, record_locators=None):
        """
        Constructor for object.

        Locator should be a selenium locator tuple '(By.ID, 'the-element-id')'
        that points to the desired <table class='PSLEVEL1SCROLLAREABODYWBO'>
        element.
        """
        self.d = driver
        driver.switch_to_default_content()
        driver.switch_to_content()
        self._loc = locator
        if record_locators:
            for k, v in record_locators.items():
                self.RECORD_LOCATORS[k] = v

    def _init_record_idx(self):
        """
        Return the index of the first record belonging to the scroll area.
        """
        if len(self.RECORD_LOCATORS.keys()) == 0:
            return 0
        else:
            for _, loc in self.RECORD_LOCATORS.items():
                pattern = "^" + r"(\d+)".join([re.escape(part)
                                              for part in loc.split("{idx}")])
                loc_fmt = loc.format(idx='')
                xpath = f".//*[starts-with(@id, '{loc_fmt}')]"
                els = self._el.find_elements_by_xpath(xpath)
                for el in els:
                    el_id = el.get_attribute('id')
                    match = re.match(pattern, el_id)
                    if match:
                        return int(match.group(1))
        raise ValueError('Could not determine initial index.')


    @property
    def _el(self):
        return self.d.find_element(*self._loc)

    @property
    def grid_counter(self):
        """
        Return the text of the PSGRIDCOUNTER element, which indicates
        the current index displayed by the grid ('1-5 of 10').
        """
        css = 'span.PSGRIDCOUNTER'
        return self._el.find_element_by_css_selector(css).text

    @property
    def pagination(self):
        """
        Returned a Pagination named tuple.

        Pagination attributes
        ---------------------
        first : int
            index of first record in view.
        last : int
            index of the last record in view.
        total : int
            total records in the scroll area.
        view_range : int
            total records displayed on current page.
        """
        Pagination = namedtuple('Pagination', ['first_in_view',
                                               'last_in_view',
                                               'total',
                                               'view_range'])
        view, _, total = self.grid_counter.split(' ')
        if '-' in view:
            first, last = [int(x) for x in view.split('-')]
        else:
            first = int(view)
            last = first
        view_range = last - (first - 1)
        total = int(total)
        return Pagination(first_in_view=first,
                          last_in_view=last,
                          total=total,
                          view_range=view_range)

    @property
    def records(self):
        """
        Generator of records within the scroll area.

        Attributes will be based on class RECORD_LOCATORS, which should be a
        dict of property names and the corresponding element id, with the
        trailing `$n` index stripped. For instance, if a record should have the
        field `test_id` and the corresponding element on the page is
        <input id='TEST_ID$0'>, the RECORD_LOCATOR entry would be
        {'test_id': 'TEST_ID'}.
        """
        self.d.switch_to_content()
        Record = namedtuple('Record', self.RECORD_LOCATORS.keys())
        init_idx = self._init_record_idx()
        for idx in range(self.pagination.view_range):
            idx += init_idx
            element_dict = {}
            for k, v in self.RECORD_LOCATORS.items():
                try:
                    el_id = v.format(idx=idx)
                    element_dict[k] = get_element(self.d, (By.ID, el_id))
                except:
                    element_dict[k] = None
            yield Record(**element_dict)

    @property
    def title(self):
        """The title of the scroll area."""
        xpath = ("//table[contains(@class, 'SCROLLAREAHEADER')]"
                 "//td[contains(@class, 'SCROLLAREAHEADER')]")
        return self._el.find_element_by_xpath(xpath).text

    def first_record(self):
        """
        Click the 'first' link to go to the first record.

        A NoSuchElementException will be raised if the link is not available.
        """
        xpath = ".//td[@class='PSGRIDNAVIGATOR']//a[contains(@id, 'htop')]"
        el = self._el.find_element_by_xpath(xpath)
        el.click()
        self.d.mp_wait()

    def last_record(self):
        """
        Click the 'last' link to go to the last record.

        A NoSuchElementException will be raised if the link is not available.
        """
        xpath = ".//td[@class='PSGRIDNAVIGATOR']//a[contains(@id, 'hend')]"
        el = self._el.find_element_by_xpath(xpath)
        el.click()
        self.d.mp_wait()

    def next_record(self):
        """
        Click the 'next' arrow to move to the next record.

        A NoSuchElementException will be raised if the link is not available.
        """
        xpath = ".//td[@class='PSGRIDNAVIGATOR']//a[contains(@id, 'hdown')]"
        el = self._el.find_element_by_xpath(xpath)
        el.click()
        self.d.mp_wait()

    def prev_record(self):
        """
        Click the 'previous' arrow to move to the previous record.

        A NoSuchElementException will be raised if the link is not available.
        """
        xpath = ".//td[@class='PSGRIDNAVIGATOR']//a[contains(@id, 'hup')]"
        el = self._el.find_element_by_xpath(xpath)
        el.click()
        self.d.mp_wait()

    def record(self, idx):
        """
        Return the record at index `idx` where idx==0 is the first record
        on the page.
        """
        self.d.switch_to_content()
        view_range = self.pagination.view_range
        if idx >= view_range:
            raise NoSuchElementException(
                f'Cannot get record {idx}. '
                f'There are only {view_range} records on the page.')
        idx += self._init_record_idx()
        Record = namedtuple('Record', self.RECORD_LOCATORS.keys())
        element_dict = {}
        for k, v in self.RECORD_LOCATORS.items():
            el_id = v.format(idx=idx)
            try:
                element_dict[k] = get_element(self.d, (By.ID, el_id))
            except Exception as e:
                element_dict[k] = None
        return Record(**element_dict)

    def view_all(self):
        """View all records within the scroll level."""
        xpath = ".//a[@class='PSLEVEL1SCROLLAREAHEADER' and text()='View All']"
        el = self._el.find_element_by_link_text('View All')
        el.click()
        self.d.mp_wait()
