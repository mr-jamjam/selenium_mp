from selenium_mp.common.utils import handle_alert


class BaseElement(object):

    def __init__(self, webelement):
        self.d = webelement.parent
        self._id = webelement.get_attribute('id')
        self._el = webelement

    def __dir__(self):
        return set(super().__dir__() + dir(self._get_element()))

    def __getattr__(self, name):
        return self._get_element().__getattribute__(name)

    def _get_element(self):
        if self._id:
            return self.d.find_element_by_id(self._id)
        else:
            return self._el

    @handle_alert
    def click(self):
        self._get_element().click()
        self.d.mp_wait()
