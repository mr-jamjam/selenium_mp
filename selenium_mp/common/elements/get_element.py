from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.remote.webelement import WebElement

from selenium_mp.common import elements


def get_element(driver, locator):
    """
    Returns a selenium_mp element object for the element found at `locator`.
    """
    if isinstance(locator, WebElement):
        el = locator
    else:
        el = WebDriverWait(driver, 1).until(
            lambda driver: driver.find_element(*locator))
    if el.tag_name.lower() == 'select':
        return elements.Dropdown(el)
    elif el.get_attribute('type') == 'checkbox':
        return elements.CheckBox(el)
    elif el.get_attribute('type') == 'button':
        return elements.Element(el)
    elif el.tag_name.lower() == 'input':
        return elements.Input(el)
    elif el.tag_name.lower() == 'textarea':
        return elements.Input(el)
    elif el.get_attribute('id') == 'pt_modals':
        return elements.Modal(driver)
    elif el.get_attribute('class').endswith('GRIDWBO'):
        return elements.Grid(driver, locator)
    elif el.get_attribute('class').endswith('SCROLLAREABODYWBO'):
        return elements.ScrollArea(driver, locator)
    else:
        return elements.Element(el)
