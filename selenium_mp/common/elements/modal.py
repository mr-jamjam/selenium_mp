from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class Modal(object):
    """
    PS Modal Dialog class.
    """


    def __init__(self, driver):
        driver.switch_to_default_content()  # modals outside of content frame
        self.d = driver
        wait = WebDriverWait(driver, 3)
        wait.until(EC.visibility_of_element_located((By.CLASS_NAME, 'PSMODAL')))

    @property
    def title(self):
        """
        The title of the modal dialog.
        """
        el = self.d.find_element_by_class_name('PSMODALHEADER')
        return el.text.strip()

    @property
    def message(self):
        """
        The message text of the modal dialog.
        """
        el = self.d.find_element_by_class_name('PSMODALCONTENT')
        return el.text.strip()

    @property
    def role(self):
        """
        The role attribute of the modal dialog.
        """
        el = self.d.find_element_by_class_name('PSMODALTABLE')
        return el.get_attribute('role')

    @property
    def buttons(self):
        """
        Generator yielding webelements for each button present in the dialog.
        """
        for el in self.d.find_elements_by_css_selector('.PSMODALCONTENT input'):
            yield el

    def click_button_by_text(self, button_text):
        """
        Click the button with value matching `button_text`.

        A NoSuchElementException will be raised if button does not exist.
        """
        xpath = ("//div[@id='pt_modals']"
                 "//input[contains(@class, 'PSPUSHBUTTON') and @value='{}']")
        self.d.find_element_by_xpath(xpath.format(button_text)).click()
        self.d.mp_wait()
        self.d.switch_to_content()

    def click_button_by_index(self, idx):
        """
        Click the button at `idx` where idx 0 is the left-most button.
        """
        xpath = ("//div[@id='pt_modals']"
                 "//input[contains(@class, 'PSPUSHBUTTON')]")
        try:
            btn = self.d.find_elements_by_xpath(xpath)[idx]
        except IndexError:
            raise NoSuchElementException(f'Button {idx} does not exist.')
        else:
            btn.click()
            self.d.mp_wait()
            self.d.switch_to_content()
