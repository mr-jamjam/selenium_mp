from collections import namedtuple
from selenium.webdriver.common.by import By
from selenium_mp.common.utils import normalize_string


class Prompt(object):
    def __init__(self, driver):
        driver.switch_to.default_content()
        self.d = driver
        self.d.switch_to_default_content()
        xpath = "//iframe[contains(@name, 'ptModFrame')]"
        self._modal_frame = driver.find_element_by_xpath(xpath)\
                                  .get_attribute('name')

    def _switch_to_default_content(self):
        self.d.switch_to.default_content()

    def _switch_to_modal(self):
        try:
            self.d.switch_to.frame(self._modal_frame)
        except:
            pass

    def clear(self):
        """Click the 'Clear' button to clear the search results."""
        self._switch_to_modal()
        self.d.find_element_by_id('#ICClear').click()
        self.d.mp_wait()

    def click_search_result(self, idx):
        """
        Click the search result at the index provided, where idx 0 corresponds
        to the first search result.
        """
        self._switch_to_modal()
        _, *trs = self.d.find_elements_by_xpath(
            "//table[@id='PTSRCHRESULTS']//tr")
        trs[idx].find_element_by_tag_name('a').click()
        self._switch_to_default_content()

    def close(self):
        """Close the modal."""
        self._switch_to_default_content()
        loc = (By.CLASS_NAME, 'PSMODALCLOSEANCHOR')
        self.d.find_element(*loc).click()
        self.d.mp_wait()
        self.d.switch_to_content()

    def search(self):
        """Click the 'Look Up' button to perform a search."""
        self._switch_to_modal()
        self.d.find_element_by_id('#ICSearch').click()
        self.d.mp_wait()

    @property
    def search_results(self):
        self._switch_to_modal()
        headers = [normalize_string(th.text)
                   for th in
                   self.d.find_elements_by_css_selector('th.PSSRCHRESULTSHDR')]
        SearchResult = namedtuple('SearchResult', headers)
        try:
            _, *trs = self.d.find_elements_by_xpath(
                "//table[@id='PTSRCHRESULTS']//tr")
        except ValueError:
            return None
        for tr in trs:
            vals = [td.text for td in tr.find_elements_by_tag_name('td')]
            yield SearchResult(*vals)

    @property
    def title(self):
        """Return the title of the modal."""
        self._switch_to_default_content()
        loc = (By.XPATH, "//div[@id='pt_modals']//span[@class='PTPOPUP_TITLE']")
        return self.d.find_element(*loc).text
