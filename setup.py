from setuptools import setup


def readme():
    """Read contents of README.md."""
    with open('README.md') as f:
        return f.read()

setup(name='selenium_mp',
      version='0.1',
      description='Tools for automating MPathways with Selenium',
      long_description=readme(),
      classifiers=[
          'Development Status :: 3 - Alpha',
          'License :: OSI Approved :: MIT License',
          'Programming Language :: Python :: 3.6',
      ],
      keywords='selenium peoplesoft mpathways',
      url='http://github.com/jamie-r-davis/selenium_mp',
      author='Jamie Davis',
      author_email='jamjam@umich.edu',
      license='MIT',
      packages=['selenium_mp'],
      install_requires=[
          'selenium',
      ],
      dependency_links=[
          'https://github.com/jamie-r-davis/mp_driver/'
      ],
      include_package_data=True,
      zip_safe=False)
